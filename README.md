# OpenML dataset: SRBCT

https://www.openml.org/d/45101

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Round blue cell tumors (SRBCT) dataset**

**Authors**: J. Khan, J. Wei, M. Ringner, L. Saal, M. Ladanyi, F. Westermann, F. Berthold, M. Schwab, C. Antonescu, C. Peterson, et al

**Please cite**: ([URL](https://www.nature.com/articles/nm0601_673)): J. Khan, J. Wei, M. Ringner, L. Saal, M. Ladanyi, F. Westermann, F. Berthold, M. Schwab, C. Antonescu, C. Peterson, et al, Classification and diagnostic prediction of cancers using gene expression profiling and artificial neural networks, Nat. Med. 7 (6) (2001) 673-679.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45101) of an [OpenML dataset](https://www.openml.org/d/45101). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45101/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45101/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45101/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

